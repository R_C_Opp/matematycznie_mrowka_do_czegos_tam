# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 10:05:09 2018

@author: MrauMrauChan
"""
import numpy as np

#Zdaniem pewnego mądrego artykułu z neta, najbardzeij optymalna liczba mrówek to 10

def InitialPheromone(dim,weights):
    return float  (dim*np.amin(weights[weights !=0]))**-1 #Początkowa wartoć w tablicy feromonów jako (<liczba wiercholkow>*<Najmniejsza odleglosc>)^-1

def PheromoneCreate(dim,number):
    pheromone=np.full((dim,dim),number)
    np.fill_diagonal(pheromone,0)
    print("\n Macierz feromonów:")
    print(pheromone)
    return pheromone

def WeightsCreate(dim,low,high):
    x=np.random.randint(low,high,(dim,dim))
    print("\n Macierz wag:")
    print(x)
    return x

def NodesCreate(dim,low,high):
    x=[]
    y=[]
    for i in np.arange(dim):
        x.append(np.random.randint(low,high))
        y.append(np.random.randint(low,high))
    print("\n Współrzędne:")
    print(x)
    print(y)
    return x,y
        
    

    
        


    