#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 15:38:14 2018

@author: JW
"""
#https://www.ics.uci.edu/~welling/teaching/271fall09/antcolonyopt.pdf
#papier na ktorym sie opieram
#wersja testowa, TSP bez powrotu 

import numpy as np
import pandas as pd
import ants_functions as fx


#matrix = pd.read_csv('path') sposob ladowania danych
#jak chcecie to mozna zczytywac podawac nazwy plikow z terminala
 
pheromone = np.full((4,4),0.5)
np.fill_diagonal(pheromone, 0)
weights = np.array([[0, 1, 1, 10],[1, 0, 10, 1], 
                    [1, 10, 0, 1], [10, 1, 1, 0]])
Q = 1       
ro = 0.5    

x=[1, 2, 1, 2]
y=[2, 2, 1, 1]

def AntColonyOptimzation(pheromone, weights, x, y, ants, Q=1, ro=0.5):
    
    best = 0
    
    for i in np.arange((5)):
        for interaion in np.arange(ants + 1):
            initial = np.random.choice(4)
            solution, objective = fx. AntConstruction(pheromone, weights, initial)
            if best == 0:
                best, winning = objective, solution 
            elif best > objective:
                    best, winning = objective, solution 
        pheromone = fx.PhreomoneUpdate(pheromone, winning,best)
        fx.DrawGraph(x, y, solution)
        print(pheromone)

AntColonyOptimzation(pheromone,weights,x, y, 10)
    
                