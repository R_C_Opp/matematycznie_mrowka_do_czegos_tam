#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 19:53:55 2018

@author: JW
"""

import numpy as np
import matplotlib.pyplot as plt
def AntConstruction(pheromone, weights, initial):
    pheromone_work = pheromone.copy()
    
    solution = []
    objective = 0
    current = initial
    pheromone_work[initial,:] = 0
    while pheromone_work.any():
        possible = pheromone_work[:,current]    
        possible = possible / possible.sum()
        choice = np.random.choice(len(possible), p=possible)
        objective += weights[current, choice]
        pheromone_work[choice,:] = 0
        solution.append([current, choice])
        current = choice
    solution.append([current, solution[0][0]])
    objective += weights[current][solution[0][0]]
    return solution, objective


def PhreomoneUpdate(pheromone, solution, objective, Q=1, ro=0.5):
    pheromone = (1 - ro)*pheromone
    summand = Q/objective
    for row, col in solution:
        pheromone[row, col] = pheromone[row, col] + summand
        pheromone[col, row] = pheromone[col, row] + summand
    return pheromone






def DrawGraph(x,y,edge_list):
    plt.plot(x, y, 'ro')
    print(edge_list)
    for row in edge_list:
            x1, x2 = x[row[0]], x[row[1]]
            y1, y2 = y[row[0]], y[row[1]]
            plt.plot([x1,x2],[y1,y2],'k-')
    plt.show()
